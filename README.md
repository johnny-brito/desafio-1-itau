# Projeto Desafio Itaú

Este projeto foi criado com o [Angular CLI](https://github.com/angular/angular-cli) versão 9.0.7.

Você pode acessar ele pelo [Gitlab](https://gitlab.com/kamikase.guy/desafio-3-itau). Este projeto segue todos os requisitos de PWA
portanto pode ser instalado em seu dispositivo Mobile ou desktop, utilizando service worker e controle de cache.

## Escopo

Um projeto Angular 9, utilizando a abordagem SPA com as seguintes funcionalidades:

* O framework utilizado deve ser um framework de Javascript.
* Seu app deve ser responsivo, usando seu Design System de prefêrencia, recomendamos o Matrial Desgin do Google https://material.angular.io/ para Angular, https://material.io/ para outros frameworks.
* A tela do seu app deve conter 3 abas **Lista Geral, Por Mês e Por Categoria.**. Exemplo [**neste link**](https://gitlab.com/desafio3/desafio-final/-/blob/master/Experiencia/Experiencia.mov).
* Só devem ser exibidos os meses em que houveram lançamentos.
* Os meses e categorias devem ser exibidos por escrito sempre.


## Tecnologias

- Angular CLI 9.0.7
- Angular material 9.2.4

## Como instalar

- Baixe ou clone este repositório usando `git clone https://gitlab.com/kamikase.guy/desafio-3-itau.git`;
- Dentro do diretório, instale as dependências usando `npm install`.

## Como executar

Execute `ng serve` para executar a versão de desenvolvimento. Depois acesse `http://localhost:4200/`.

## Como compilar/construir

Execute `ng build` para buildar o projeto. Para buildar a versão de produção adicione a flag `--prod`. Os arquivos serão armazenados do diretório `dist`.

## Dúvidas
Caso há alguma dúvida em relação a este repositório, envie para gior.grs@gmail.com.
