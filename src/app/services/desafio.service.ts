import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Lancamento } from '../models/lancamento';
import { map } from 'rxjs/operators'
import { Categoria } from '../models/Categoria';

@Injectable({
  providedIn: 'root'
})
export class DesafioService {

  constructor(private http: HttpClient) { }



  getLancamentos() {
    return this.http.get<Lancamento[]>(`${environment.apiUrl}lancamentos`);
  }

  getCategorias() {
    return this.http.get<Categoria[]>(`${environment.apiUrl}categorias`);
  }
}
