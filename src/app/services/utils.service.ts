import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  monthsList = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
    'Julho', 'Augosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];

  constructor() { }

  table1Data(lancamentos, categorias) {
    return lancamentos.map(lancamento => {
      return {
        id: lancamento.id,
        valor: lancamento.valor,
        origem: lancamento.origem,
        categoria: categorias.find(el => el.id === lancamento.categoria).nome,
        mes_lancamento: lancamento.mes_lancamento,
        nome_mes: this.monthsList[lancamento.mes_lancamento - 1]
      };
    });
  }


  table2Data(lancamentos) {
    const gastoMes = [];

    this.monthsList.forEach(mes => {
      let valorTotal = 0;
      lancamentos.map(data => {
        if (data.nome_mes === mes) {
          valorTotal = valorTotal + data.valor;
        }
      });
      if (valorTotal > 0) {
        gastoMes.push({
          nome_mes: mes,
          total_gasto: valorTotal.toLocaleString('pt-BR', { minimumFractionDigits: 2, style: 'currency', currency: 'BRL' })
        });
      }
    });

    return gastoMes;
  }

  table3Data(categorias, lancamentos) {
    let valorTotal = 0;
    return categorias.map(categoria => {
      lancamentos.map(data => {
        if (data.categoria === categoria.id) {
          valorTotal = valorTotal + data.valor;
        }
      });
      if (valorTotal > 0) {
        return {
          nome: categoria.nome,
          total_gasto: valorTotal.toLocaleString('pt-BR', { minimumFractionDigits: 2, style: 'currency', currency: 'BRL' })
        };
      }
    });

  }



}
