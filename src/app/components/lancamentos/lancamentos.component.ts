import { Component, OnInit } from '@angular/core';
import { DesafioService } from 'src/app/services/desafio.service';
import { Lancamento } from 'src/app/models/lancamento';
import { Categoria } from 'src/app/models/Categoria';
import { UtilsService } from 'src/app/services/utils.service';
import { LancamentoTable } from 'src/app/models/LancamentoTable';
import { GastoMes } from 'src/app/models/GastoMes';
import { GastoCategoria } from 'src/app/models/GastoCategoria';

@Component({
  selector: 'app-lancamentos',
  templateUrl: './lancamentos.component.html',
  styleUrls: ['./lancamentos.component.css']
})
export class LancamentosComponent implements OnInit {

  public lancamentos: Lancamento[];
  public categorias: Categoria[];

  public table1Data: LancamentoTable[];
  table1Columns: string[] = ['origem', 'valor', 'nome_mes'];

  public table2Data: GastoMes[];
  table2Columns: string[] = ['nome_mes', 'total_gasto'];

  public table3Data: GastoCategoria[];
  table3Columns: string[] = ['nome', 'total_gasto'];

  constructor(
    private service: DesafioService,
    private utils: UtilsService) {
  }

  ngOnInit(): void {
    this.getLancamentos();
  }

  getLancamentos() {
    this.service.getLancamentos().subscribe(
      (res: Lancamento[]) => {
        this.lancamentos = res;
        this.getCategorias();
        console.log(res);
      }, error => {
        console.error(error);
      }
    );
  }

  getCategorias() {
    this.service.getCategorias().subscribe(
      (res: Categoria[]) => {
        this.categorias = res;
        console.log(res);
        this.mountData();
      }, error => {
        console.error(error);
      }
    );
  }

  mountData() {
    this.table1Data = this.utils.table1Data(this.lancamentos, this.categorias);
    this.table2Data = this.utils.table2Data(this.table1Data);
    this.table3Data = this.utils.table3Data(this.categorias, this.lancamentos);
  }

}
