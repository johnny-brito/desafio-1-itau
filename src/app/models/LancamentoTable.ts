export interface LancamentoTable {
    id: number;
    valor: number;
    origem: string;
    categoria: number;
    mes_lancamento: number;
    nome_mes: string;
}
